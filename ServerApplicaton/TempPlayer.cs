﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServerApplicaton.Domain2.Entities;

namespace ServerApplicaton
{
    public class TempPlayer
    {
        public int Room;
        public bool InMatch;
        public int Castbar;
        public int Mana;

        public List<CardServer> Deck { get; set; }
        public List<CardServer> HandCards { get; set; }
        public List<CardServer> BattleCards { get; set; }
        public int NrOfShields { get; set; }
        public bool AddedManaThisTurn { get; set; }

        public TempPlayer()
        {
            AddedManaThisTurn = false;
            Deck = new List<CardServer>();
            HandCards = new List<CardServer>();
            BattleCards = new List<CardServer>();
            Mana = 0;
            NrOfShields = 5;
        }

        public int DrawCard()
        {
            var randomIndex = new Random().Next(Deck.Count);

            var card = Deck.ElementAt(randomIndex);

            Deck.Remove(card);
            HandCards.Add(card);

            return (int)card.Id;
        }

        public bool SummonCard(int cardId)
        {
            var card = HandCards.SingleOrDefault(c => c.Id == cardId);

            if (card == null || Mana < card.ManaCost)
            {
                return false;
            }

            HandCards.Remove(card);

            if (card.Type == "Creature")
            {
                BattleCards.Add(card);
            }

            Mana -= card.ManaCost;

            return true;
        }

        public bool AddMana(int cardId)
        {
            var card = HandCards.SingleOrDefault(c => c.Id == cardId);

            if (card == null && AddedManaThisTurn == false)
            {
                return false;
            }

            HandCards.Remove(card);
            
            Mana++;

            AddedManaThisTurn = true;

            return true;
        }

        public void DestroyMonster(int cardId)
        {
            var card = BattleCards.SingleOrDefault(c => c.Id == cardId);

            if (card != null)
            {
                BattleCards.Remove(card);
            }
        }

        public void EndPlayerTurn()
        {
            AddedManaThisTurn = false;
        }

        public CardServer GetCardFromHand(long cardId)
        {
            return HandCards.SingleOrDefault(c => c.Id == cardId);
        }
    }
}
