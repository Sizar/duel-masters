﻿namespace ServerApplicaton.Enums
{
    public enum Races
    {
        Dragonoid,
        ArmoredDragon,
        Human,
        ArmoredWyvern,
        FireBird,
        RockBeast
    }
}
