﻿using ServerApplicaton.Domain2.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ServerApplicaton
{
    class ServerSendData
    {
        private int turnIndex = 0;

        public static ServerSendData instance = new ServerSendData();

        public void SendDataToRoom(int room, byte[]data)
        {
            for(int i = 0; i < 2; i++)
            {
                if(RoomInstance._room[room].player[i] > 0)
                {
                    SendDataTo(RoomInstance._room[room].player[i], data);
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        public void SendDataTo(int index, byte[] data)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(data);
            Network.Clients[index].myStream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
            buffer = null;
        }

        public void SendIngame(int index)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(1);

            //Player Data
            buffer.WriteString(Network.Player[index].Username);
            buffer.WriteString(Network.Player[index].Password);
            buffer.WriteInteger(Network.Player[index].Level);
            buffer.WriteByte(Network.Player[index].Access);
            buffer.WriteByte(Network.Player[index].FirstTime);

            SendDataTo(index, buffer.ToArray());
            buffer = null;
        }

        public void SendMatchMaking(int room)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(3);
            buffer.WriteString(Network.Player[RoomInstance._room[room].player[0]].Username);
            buffer.WriteString(Network.Player[RoomInstance._room[room].player[1]].Username);
            SendDataToRoom(room, buffer.ToArray());
            buffer = null;
            Network.TempPlayer[RoomInstance._room[room].player[0]].inMatch = true;
            Network.TempPlayer[RoomInstance._room[room].player[1]].inMatch = true;
            for (int i = 0; i < 5; ++i)
            {
                System.Threading.Thread.Sleep(100);
                SendCardToPlayer(RoomInstance._room[room].player[1]);
                System.Threading.Thread.Sleep(100);
                SendOpponentDrawCard(RoomInstance._room[room].player[1]);
                System.Threading.Thread.Sleep(100);
                SendCardToPlayer(RoomInstance._room[room].player[0]);
                System.Threading.Thread.Sleep(100);
                SendOpponentDrawCard(RoomInstance._room[room].player[0]);
                System.Threading.Thread.Sleep(100);
            }

            SendTurnStatus(RoomInstance._room[room].player[turnIndex], 1);
            SendTurnStatus(RoomInstance._room[room].player[1-turnIndex], 0);
            turnIndex = 1 - turnIndex;

        }
        public void SendOpponentDrawCard(int index)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(6); //send id card
            
            SendDataTo(index, buffer.ToArray());
            buffer = null;
        }

        public void SendTurnStatus(int index,int status)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(7); //send id card
            buffer.WriteInteger(status);
            SendDataTo(index, buffer.ToArray());
            buffer = null;
        }

        public void SendRefreshBar(int index)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(4);
            buffer.WriteInteger(Network.TempPlayer[index].Castbar);
            SendDataTo(index, buffer.ToArray());
            buffer = null;
        }

        public void SendCardToPlayer(int index)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(5);
            int drawnCardId = Network.TempPlayer[index].DrawCard();
            buffer.WriteInteger(drawnCardId);
            SendDataTo(index, buffer.ToArray());

            buffer.Dispose();
        }
        public void SendMoveCard(int index,int id,float x, float y, float z)
        {
            Console.WriteLine("Sending Move card to "+index.ToString());
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteInteger(8);
            buffer.WriteInteger(id);
            buffer.WriteFloat(x);
            buffer.WriteFloat(y);
            buffer.WriteFloat(z);
            SendDataTo(index, buffer.ToArray());
            Console.WriteLine("Server move sending: " + x.ToString() + ", " + y.ToString() + ", " + z.ToString());

            buffer.Dispose();
        }


        private byte[] ObjectToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        private T FromByteArray<T>(byte[] data)
        {
            if (data == null)
                return default(T);
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(data))
            {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }

        public void SendDestroyedMonsterToPlayer(int index, int cardId)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();

            //TODO client add 11 too
            buffer.WriteInteger(11);
            buffer.WriteInteger(cardId);

            SendDataTo(index, buffer.ToArray());

            buffer.Dispose();
        }

        public void SendDestroyedMonsterToOpponent(int index, int cardId)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();

            //TODO client add 12 too
            buffer.WriteInteger(12);
            buffer.WriteInteger(cardId);

            SendDataTo(index, buffer.ToArray());

            buffer.Dispose();
        }
    }
}
