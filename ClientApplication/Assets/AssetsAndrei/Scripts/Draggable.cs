﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour 
{
    Vector3 dist;
    float posx;
    float posy;
    Vector3 initialPos;
    float closeWhenPress = 150.0f;
    float cardDistance = 10.0f;
    public int pushDirection=-1;
    public int positionInPlacehold = -1;
    public int MoveStatus = 0;
    //private GameObject initialParent;

    void OnMouseDown()
    {
        if(tag!="DeckCard")
        {
            initialPos = transform.position;
            dist = Camera.main.WorldToScreenPoint(transform.position);
            posx = Input.mousePosition.x - dist.x;
            posy = Input.mousePosition.y - dist.y;
            GetComponent<Rigidbody>().MovePosition(new Vector3(transform.position.x, transform.position.y + closeWhenPress, transform.position.z));
        if(positionInPlacehold>=0)
        {
            transform.parent.gameObject.GetComponent<HandZone>().SpaceStatus[positionInPlacehold] = 0;
        }
        
        }
       
        
       // Debug.Log("Mouse down");
    }

    void OnMouseDrag()
    {
        if(tag =="Card")
        {
            Vector3 curPos = new Vector3(Input.mousePosition.x - posx, Input.mousePosition.y - posy, dist.z);
            Vector3 worldpos = Camera.main.ScreenToWorldPoint(curPos);
            worldpos.y += closeWhenPress;
            //transform.position = worldpos;
            GetComponent<Rigidbody>().MovePosition(worldpos);
            //Debug.Log("Mouse drag");
            MoveStatus = 1;
        }

    }
    private void OnMouseUp()
    {
        if(tag!="DeckCard")
        {
            Vector3 position = transform.position;
            position.y = initialPos.y;
            GetComponent<Rigidbody>().MovePosition(position);
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        int direction = -1;
        if(other.tag == "BattleCard")
        {
            direction = other.GetComponent<Draggable>().pushDirection;
        }
        if (direction == 5)
        {
            Debug.Log("Triggered sursa " + name +" cu "+ other.name);
            Vector3 bounds = other.GetComponent<Renderer>().bounds.size;
            Vector3 position = transform.position;
            if(direction==0)
            {
                if (other.transform.position.x < position.x)
                {
                    position.x = other.transform.position.x - bounds.x - cardDistance;
                    pushDirection = 1;
                }
                else
                {
                    position.x = other.transform.position.x + bounds.x + cardDistance;
                    pushDirection = 3;
                }
            }
            else
            {
                pushDirection = direction;
                direction -= 2;
                position.x = other.transform.position.x + bounds.x* direction + cardDistance* direction;
            }
            
                
           // }
            transform.position = position;
            //GetComponent<Rigidbody>().MovePosition(position);

            other.GetComponent<Draggable>().pushDirection = -1;



        }

    }


}
