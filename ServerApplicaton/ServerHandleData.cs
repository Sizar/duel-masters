﻿using System;
using System.Collections.Generic;

namespace ServerApplicaton
{
    class ServerHandleData
    {
        public static ServerHandleData instance = new ServerHandleData();
        private delegate void Packet_(int Index, byte[] Data);
        private Dictionary<int, Packet_> Packets;

        public void InitMessages()
        {
            Packets = new Dictionary<int, Packet_>();
            Packets.Add(1, HandleNewAccount);
            Packets.Add(2, HandleLogin);
            Packets.Add(3, HandleLookingForMatch);
            Packets.Add(4, HandleCancelLookingForMatch);
            Packets.Add(5, HandlePlayerEndTurn);
            Packets.Add(6, HandlePlayerSummonsCard);
            Packets.Add(7, HandleMoveCard);

        }

        private void HandleMoveCard(int Index, byte[] Data)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();

            int id = buffer.ReadInteger();
            float x = buffer.ReadFloat();
            float y = buffer.ReadFloat();
            float z = buffer.ReadFloat();
            Console.WriteLine("Server move received: " + id.ToString() + " - " + x.ToString()+", " + y.ToString() + ", " + z.ToString() );
            RoomInstance.instance.MoveCard(Index,id,x,y,z);
        }

        private void HandlePlayerAddsMana(int Index, byte[] Data)
        {
            Console.WriteLine("Player attacks a monster");
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();

            var currentPlayer = Network.TempPlayer[Index];

            int cardIdForMana = buffer.ReadInteger();

            var result = currentPlayer.AddMana(cardIdForMana);
        }

        private void HandlePlayerAttackMonster(int Index, byte[] Data)
        {
            Console.WriteLine("Player attacks a monster");
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();

            int attackingCardId = buffer.ReadInteger();
            int defendingCardId = buffer.ReadInteger();

            var roomPlayer1Index = RoomInstance._room[Network.TempPlayer[Index].Room].player[0];
            var roomPlayer2Index = RoomInstance._room[Network.TempPlayer[Index].Room].player[1];

            var opponentIndex = roomPlayer1Index == Index ? roomPlayer2Index : roomPlayer1Index;

            var currentPlayer = Network.TempPlayer[Index];
            var opponentPlayer = Network.TempPlayer[opponentIndex];

            var attackingCard = currentPlayer.GetCardFromHand(attackingCardId);
            var defendingCard = opponentPlayer.GetCardFromHand(defendingCardId);

            if (attackingCard == null || defendingCard == null)
            {
                return; // TODO
            }

            if (attackingCard.Power > defendingCard.Power)
            {
                opponentPlayer.DestroyMonster(defendingCardId);

                ServerSendData.instance.SendDestroyedMonsterToPlayer(Index, defendingCardId);
                ServerSendData.instance.SendDestroyedMonsterToOpponent(opponentIndex, defendingCardId);
            }
            else if (attackingCard.Power < defendingCard.Power)
            {
                currentPlayer.DestroyMonster(attackingCardId);

                ServerSendData.instance.SendDestroyedMonsterToPlayer(Index, attackingCardId);
                ServerSendData.instance.SendDestroyedMonsterToOpponent(opponentIndex, attackingCardId);
            }
            else
            {
                currentPlayer.DestroyMonster(attackingCardId);
                opponentPlayer.DestroyMonster(defendingCardId);

                ServerSendData.instance.SendDestroyedMonsterToPlayer(Index, attackingCardId);
                ServerSendData.instance.SendDestroyedMonsterToOpponent(opponentIndex, attackingCardId);

                ServerSendData.instance.SendDestroyedMonsterToPlayer(Index, defendingCardId);
                ServerSendData.instance.SendDestroyedMonsterToOpponent(opponentIndex, defendingCardId);
            }
        }

        private void HandlePlayerAttackOpponent(int Index, byte[] Data)
        {
            Console.WriteLine("Player attacks an opponent");
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();

            int attackingCardId = buffer.ReadInteger();

            var roomPlayer1Index = RoomInstance._room[Network.TempPlayer[Index].Room].player[0];
            var roomPlayer2Index = RoomInstance._room[Network.TempPlayer[Index].Room].player[1];

            var opponentIndex = roomPlayer1Index == Index ? roomPlayer2Index : roomPlayer1Index;

            var currentPlayer = Network.TempPlayer[Index];
            var opponentPlayer = Network.TempPlayer[opponentIndex];

            var attackingCard = currentPlayer.GetCardFromHand(attackingCardId);

            if (attackingCard == null)
            {
                return;
            }

            if (opponentPlayer.NrOfShields == 0)
            {
                return;
            }

            if (opponentPlayer.NrOfShields - attackingCard.NrOfShieldsItBreaks < 0)
            {
                opponentPlayer.NrOfShields = 0;
            }
            else
            {
                opponentPlayer.NrOfShields = opponentPlayer.NrOfShields - attackingCard.NrOfShieldsItBreaks.Value;
            }
        }

        private void HandlePlayerSummonsCard(int Index, byte[] Data)
        {
            Console.WriteLine("End Turn had been pressed!");
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();
            RoomInstance.instance.EndTurn(Index);
        }

        private void HandlePlayerEndTurn(int Index, byte[] Data)
        {
            Console.WriteLine("End Turn had been pressed!");
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();
            RoomInstance.instance.EndTurn(Index);
        }

        private void HandleCancelLookingForMatch(int index, byte[] Data)
        {
            Console.WriteLine("Cancel had been pressed!");
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();
            RoomInstance.instance.LeaveRoom(index);
        }

        public void HandleData(int index, byte[] data)
        {
            int packetnum;
            Packet_ Packet;
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(data);
            packetnum = buffer.ReadInteger();
            buffer = null;
            //Console.WriteLine(packetnum);
            if (packetnum == 0)
                return;

            if (Packets.TryGetValue(packetnum, out Packet))
            {
                Packet.Invoke(index, data);
            }

        }

        void HandleNewAccount(int index, byte[] Data)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();
            string username = buffer.ReadString();
            string password = buffer.ReadString();

            if (Database.instance.AccountExist(username) == true)
            {
                //SendAlertMsg
                return;
            }


            Database.instance.AddAccount(index, username, password);

            ServerSendData.instance.SendIngame(index);
        }

        void HandleLogin(int index, byte[] Data)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();
            string username = buffer.ReadString();
            string password = buffer.ReadString();

           
            if (Database.instance.AccountExist(username) == false)
            {
                Console.WriteLine("Accont doesn't exists.");
                //SendAlertMsg user does not exist
                return;
            }

            if(Database.instance.PasswordOK(username,password) == false)
            {
                //SendAlertMsg password does not match
                return;
            }
            Database.instance.LoadPlayer(index, username);
            ServerSendData.instance.SendIngame(index);
        }

        void HandleLookingForMatch(int index, byte[] Data)
        {
            KaymakGames.KaymakGames buffer = new KaymakGames.KaymakGames();
            buffer.WriteBytes(Data);
            int packet = buffer.ReadInteger();
            RoomInstance.instance.JoinOrCreateRoom(index);
        }
    }
}
