﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {

    public int id;

    public string cardName;
    public enum Civilization { fire, darkness, water, nature, light };
    public Civilization civilization;
    public string race;
    public enum CardType {creature,spell};
    public CardType cardType;
    public List<string> specialAbilities;

    public int power;
    public int manaCost;
    public int nrShieldsToBrake;

    public GameObject frontImage;
    public GameObject backImage;  
    


	// Use this for initialization
	void Start () {

        List<Card> deck = new List<Card>();
        Card card1 = new Card();
        card1.id = 1;
        card1.cardName = "Brawler Zyler";
        card1.civilization = Civilization.fire;
        card1.race = "Human";
        card1.cardType = CardType.creature;
        card1.power = 1000;
        card1.manaCost = 2;
        card1.nrShieldsToBrake = 1;
        card1.specialAbilities.Add("Power attacker +2000 (While attacking, this creature gets +2000 power.)");


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
