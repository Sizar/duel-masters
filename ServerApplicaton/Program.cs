﻿using System;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading;
using ServerApplicaton.Domain2;
using ServerApplicaton.Domain2.Entities;
using ServerApplicaton.Enums;

namespace ServerApplicaton
{
    public class Program
    {
        private static Thread threadConsole;
        private static bool consoleRunning;

        static void Main(string[] args)
        {
            threadConsole = new Thread(new ThreadStart(ConsoleThread));
            threadConsole.Start();

            //MySQL.mysql.MySQLInit();
            Database.instance.CheckPath(Database.instance.PATH_ACCOUNT);
            ServerHandleData.instance.InitMessages();
            Network.instance.ServerStart();
           // GameLogic.instance.ServerLoop();
        }

        private static void ConsoleThread()
        {
            string line;
            consoleRunning = true;

            while (consoleRunning)
            {
                line = Console.ReadLine();

                if (String.IsNullOrWhiteSpace(line))
                {
                    consoleRunning = false;
                    return;
                }

                else
                {

                }
            }
        }
    }
}
