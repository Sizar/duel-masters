﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using ServerApplicaton.Domain2.Entities;

namespace ServerApplicaton.Domain2
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base(nameOrConnectionString: "Default") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<CardServer> Cards { get; set; }
        public DbSet<CardDeck> CardDeck { get; set; }
        public DbSet<Deck> Decks { get; set; }
        public DbSet<PlayerDb> PlayerDbs { get; set; }
    }
}
