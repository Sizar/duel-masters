﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerApplicaton.Domain2.Entities
{
    [Table("CardDeck")]
    public class CardDeck
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public long CardId { get; set; }
        public long DeckId { get; set; }
    }
}
