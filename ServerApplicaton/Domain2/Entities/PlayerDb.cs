﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerApplicaton.Domain2.Entities
{
    [Table("Players")]
    public class PlayerDb
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
    }
}
