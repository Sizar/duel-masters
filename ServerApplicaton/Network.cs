﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using ServerApplicaton.Domain2;
using ServerApplicaton.Domain2.Entities;

namespace ServerApplicaton
{
    class Network
    {
        public TcpListener ServerSocket;
        public static Network instance = new Network();
        public static Client[] Clients = new Client[100];
        public static Player[] Player = new Player[100];
        public static TempPlayer[] TempPlayer = new TempPlayer[100];

        public void ServerStart()
        {
            var deck = new List<CardServer>();

            using (var context = new AppDbContext())
            {
                var playerDeck = context.Decks.SingleOrDefault(d => d.PlayerId == 1);
                var playerCardsIds = context.CardDeck
                    .Where(cd => cd.DeckId == playerDeck.Id)
                    .Select(cd => cd.CardId)
                    .ToList();
                deck.AddRange(context.Cards.Where(c => playerCardsIds.Contains(c.Id)));
            }

            for (int i = 1; i < 100; i++)
            {
                Clients[i] = new Client();
                Player[i] = new Player();
                TempPlayer[i] = new TempPlayer();
                TempPlayer[i].Deck = new List<CardServer>(deck);
                RoomInstance._room[i] = new Room();
            }

            ServerSocket = new TcpListener(IPAddress.Any, 5500);
            ServerSocket.Start();
            ServerSocket.BeginAcceptTcpClient(OnClientConnect, null);
            Console.WriteLine("Server has successfully started.");
        }

        void OnClientConnect(IAsyncResult result)
        {
            TcpClient client = ServerSocket.EndAcceptTcpClient(result);
            client.NoDelay = false;
            ServerSocket.BeginAcceptTcpClient(OnClientConnect, null);
            
            for(int i = 1; i < 100; i++)
            {
                if(Clients[i].Socket == null)
                {
                    Clients[i].Socket = client;
                    Clients[i].Index = i;
                    Clients[i].IP = client.Client.RemoteEndPoint.ToString();
                    Clients[i].Start();
                    Console.WriteLine("Incoming Connection from " + Clients[i].IP + "|| Index: " + i);
                    //SendWelcomeMessages
                    return;
                }
            }

        }
    }
}
