﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerApplicaton.Domain2.Entities
{
    [Serializable]
    [Table("Cards")]
    public class CardServer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string Name { get; set; }
       
        public string Civilization { get; set; }

        public string Race { get; set; }

        public string Type { get; set; }

        public string SpecialAbility1 { get; set; }

        public string SpecialAbility2 { get; set; }        

        public int? Power { get; set; }

        public int ManaCost { get; set; }

        public int? NrOfShieldsItBreaks { get; set; }

        public byte[] FrontImage{ get; set; }
    }
}
