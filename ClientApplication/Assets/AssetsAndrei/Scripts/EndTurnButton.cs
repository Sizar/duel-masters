﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurnButton : MonoBehaviour {

    public GameObject card;
    public Vector3 spawnPosition;
    public Vector3 enemySpawnPosition;
    private int newCardAnimation = 0;
    private List<GameObject> allyNewCard = new List<GameObject>();
    private List<GameObject> enemyNewCard = new List<GameObject>();
    public float speed = 0.5f;
    public float allyxThreshold = 100;
    public float enemyxThreshold = 400;
    private List<int> allyySign = new List<int>();
    private List<int> enemyySign = new List<int>();

    public int manaCardPut = 0;
	// Use this for initialization
	void Start () {
        spawnPosition = new Vector3(458, 38, 73);
        enemySpawnPosition = new Vector3(80, 68, 392);
        //card = Resources.Load("Card") as GameObject;

    }

    private void OnMouseDown()
    {

        GameObject newCard = Instantiate(card,spawnPosition,new Quaternion(0,0,0,0));
        allyNewCard.Add(newCard);
        newCardAnimation = 1;
        allyySign.Add(1);
        enemyySign.Add(1);
        manaCardPut = 0;
        //myNewCard.transform.position =spawnPosition

    }

    private void Update()
    {
        
        if (newCardAnimation>0)
        {
            for(int i = allyNewCard.Count-1; i>=0; --i)
            {
                Debug.Log("Ally animation" + allyNewCard.Count.ToString() + " " + newCardAnimation.ToString());
                GameObject newCard = allyNewCard[i];
                Vector3 NCposition = newCard.transform.position;
                Transform NCparent = newCard.transform.parent;
                if (NCposition.x < allyxThreshold || NCparent != null)
                {
                    newCardAnimation--;
                    allyNewCard.RemoveAt(i);
                    allyySign.RemoveAt(i);
                    Debug.Log("Ally removed" + allyNewCard.Count.ToString() + " " + newCardAnimation.ToString());
                    if (NCparent != null)
                    {
                        //parent.GetComponent<HandZone>().cardNumbers++;
                        //parent.GetComponent<HandZone>().InitPlaceholders();
                    }
                }
                else
                {
                    NCposition.x -= speed;
                    if (NCposition.y > 150)
                    {
                        allyySign[i] = -1;
                    }
                    NCposition.y += speed * allyySign[i];


                    newCard.GetComponent<Rigidbody>().MovePosition(NCposition);
                }
            }

            for (int i = enemyNewCard.Count - 1; i >= 0; --i)
            {
               
                GameObject newCard = enemyNewCard[i];
                Vector3 NCposition = newCard.transform.position;
                Transform NCparent = newCard.transform.parent;
                Debug.Log("enemy animation" + allyNewCard.Count.ToString() + " " + newCardAnimation.ToString());
                if (NCposition.x > enemyxThreshold || NCparent != null)
                {
                    newCardAnimation--;
                    enemyNewCard.RemoveAt(i);
                    enemyySign.RemoveAt(i);
                    Debug.Log("Enemy removed" + allyNewCard.Count.ToString() + " " + newCardAnimation.ToString());
                    if (NCparent != null)
                    {
                        //parent.GetComponent<HandZone>().cardNumbers++;
                        //parent.GetComponent<HandZone>().InitPlaceholders();
                    }
                }
                else
                {
                    NCposition.x += speed;
                    if (NCposition.y > 150)
                    {
                        enemyySign[i] = -1;
                    }
                    NCposition.y += speed * enemyySign[i];


                    newCard.GetComponent<Rigidbody>().MovePosition(NCposition);
                }
            }

        }
    }

    public void DrawAllyCard(int id)
    {
        GameObject newCard = Instantiate(card, spawnPosition, new Quaternion(0, 0, 0, 0));
        allyNewCard.Add(newCard);
        allyySign.Add(1);
        newCardAnimation++;
        Debug.Log("Ally : "+allyNewCard.Count.ToString() + " " + newCardAnimation.ToString());

    }
    public void DrawEnemyCard()
    {
        GameObject newCard = Instantiate(card, enemySpawnPosition, new Quaternion(0, 0, 0, 0));
        enemyNewCard.Add(newCard);
        enemyySign.Add(1);
        newCardAnimation++;
        Debug.Log("Enemy : "+enemyNewCard.Count.ToString() + " " + newCardAnimation.ToString());

    }


}
