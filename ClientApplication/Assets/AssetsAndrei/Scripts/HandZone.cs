﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandZone : MonoBehaviour {

    // Use this for initialization
    public enum ZoneType { HandZone,BattleZone,ManaZone,EnemyHandZone,EnemyBattleZone,EnemyManaZone};
    public ZoneType zone;
    public List<Vector3> placeholders = new List<Vector3>();
    public List<int> SpaceStatus = new List<int>();
    public float cardDistance = 50;
    public int cardNumbers = 8;

    public void InitPlaceholders()
    {
        cardNumbers++;
        Vector3 bounds = gameObject.GetComponent<Renderer>().bounds.size;
        cardDistance = bounds.x / 8;
        float xStart = (transform.position.x - bounds.x / 2) + 15.0f;


        Vector3 initialCard = new Vector3(xStart, 5, transform.position.z);
        
        for (int i = placeholders.Count; i <= cardNumbers; i++)
        {
            placeholders.Add(initialCard);
            SpaceStatus.Add(0);
            initialCard.x += cardDistance;
        }
    }

    private void Start()
    {
        InitPlaceholders();
        
    }

    private void OnTriggerEnter(Collider other)
    {

        EndTurnButton endTurn = GameObject.FindGameObjectsWithTag("EndTurn")[0].GetComponent<EndTurnButton>();
        bool isManaAvaible = true;
        if(zone == ZoneType.ManaZone || zone == ZoneType.EnemyManaZone)
        {
            isManaAvaible = false;
            if (endTurn.manaCardPut==0)
                isManaAvaible = true;
        }
        


        if(other.tag=="Card" && isManaAvaible  )
        {
            //Debug.Log("Object entered " + other.name);
            other.transform.SetParent(transform);
            Vector3 bounds = gameObject.GetComponent<Renderer>().bounds.size;
            Vector3 position = other.transform.position;
            position.z = transform.position.z;
            Debug.Log(other.name + "on "+name);
            other.transform.position = position;
            //other.GetComponent<Rigidbody>().MovePosition(position);
            if (gameObject.GetComponent<HandZone>().zone == ZoneType.BattleZone)
            {
                other.tag = "BattleCard";
            }
            else
            {
                other.tag = "Card";
            }
            other.GetComponent<Draggable>().pushDirection = 0;
            other.GetComponent<Draggable>().MoveStatus = 0;
            Vector3 newCardBound = other.GetComponent<Renderer>().bounds.size;

            int minPositionIndex = -1;
            for(int i=0; i<SpaceStatus.Count; ++i)
                if(SpaceStatus[i]==0)
                {
                    minPositionIndex = i;
                    break;
                }
            if(minPositionIndex==-1)
            {
                minPositionIndex = SpaceStatus.Count;
                InitPlaceholders();
                
            }
            for(int i=0; i<cardNumbers; i++)
            {
                if(Vector3.Distance(other.transform.position,placeholders[i]) <
                   Vector3.Distance(other.transform.position, placeholders[minPositionIndex]) && SpaceStatus[i] ==0)
                {
                    minPositionIndex = i;
                }
            }
            SpaceStatus[minPositionIndex] = 1;
            other.GetComponent<Rigidbody>().MovePosition(placeholders[minPositionIndex]);
            other.GetComponent<Draggable>().positionInPlacehold = minPositionIndex;
            
            if(zone==ZoneType.ManaZone || zone == ZoneType.EnemyManaZone)
            {
                Quaternion rotation = other.transform.rotation;
                rotation.y += 180;
                other.transform.rotation = rotation;
                other.tag = "ManaCard";
                endTurn.manaCardPut = 1;
               // other.GetComponent<Rigidbody>().MoveRotation(rotation);
            }

            //GameObject[] objs;
            //objs = GameObject.FindGameObjectsWithTag("BattleCard");
            //foreach (GameObject otherCards in objs)
            //{
            //    bounds = otherCards.GetComponent<Renderer>().bounds.size;
            //    if(other.transform)
            //    lightuser.GetComponent<light>().enabled = false;
            //}

        }
        

    }


}
