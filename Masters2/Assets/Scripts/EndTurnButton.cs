﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurnButton : MonoBehaviour {

    public GameObject card;
    public Vector3 spawnPosition;
    private int newCardAnimation = 0;
    private GameObject newCard;
    public float speed = 0.5f;
    public float xThreshold = 100;
    private int ySign = 1;

    public int manaCardPut = 0;
	// Use this for initialization
	void Start () {
        spawnPosition = new Vector3(458, 38, 73);
        //card = Resources.Load("Card") as GameObject;

    }

    private void OnMouseDown()
    {

        newCard = Instantiate(card,spawnPosition,new Quaternion(0,0,0,0));
        newCardAnimation = 1;
        ySign = 1;
        manaCardPut = 0;
        //myNewCard.transform.position =spawnPosition

    }

    private void Update()
    {
        
        if (newCardAnimation==1)
        {

            Vector3 position = newCard.transform.position;
            Transform parent = newCard.transform.parent;
            if (position.x < xThreshold || parent != null)
            {
                newCardAnimation = 0;
                if (parent != null)
                {
                    //parent.GetComponent<HandZone>().cardNumbers++;
                    //parent.GetComponent<HandZone>().InitPlaceholders();
                }
            }
            else
            {
                position.x -= speed;
                if (position.y > 150)
                {
                    ySign = -1;
                }
                position.y += speed * ySign;


                newCard.GetComponent<Rigidbody>().MovePosition(position);
            }
            
            
        }
    }

}
